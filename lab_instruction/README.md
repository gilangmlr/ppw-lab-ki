# LAB Instructions
 - Lab 1: Introduction to Git (on GitLab) & TDD (_Test-Driven Development_) with Django

 - Lab 2: Introduction to _Django Framework_

 - Lab 3: PostgreSQL _Setting_ in Heroku, introduction to _models_ and TDD

 - Lab 4: Introduction to _HTML5_


